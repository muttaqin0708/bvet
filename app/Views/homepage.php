<?= $this->extend('master') ?>

<?= $this->section('main') ?>
<div style="padding-top: 0px; margin-top:0px"></div>
<div class="containter p-4">
    <!-- Carousel -->
    <div style="background-color: #1C400B;">
        <div id="carouselExample" class="carousel slide mb-5" style="width: 100%; margin: 0 auto;">
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="d-block w-70 img-fluid mx-auto" alt="Image 1" style="width: 70%; height: 56.25vw; max-height: 100vh;">
            </div>
            <div class="carousel-item">
            <img src="https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2021/06/14042550/Kesehatan-Anak.jpg" class="d-block w-70 img-fluid mx-auto" alt="Image 2" style="width: 70%; height: 56.25vw; max-height: 100vh;">
            </div>
            <div class="carousel-item">
            <img src="https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2021/06/14042550/Kesehatan-Anak.jpg" class="d-block w-70 img-fluid mx-auto" alt="Image 3" style="width: 70%; height: 56.25vw; max-height: 100vh;">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
        </div>
    </div>

    <!-- Card Info dan Image/video -->
    <div class="row justify-content-center d-flex">
        <div class="col-12 col-md-6 col-lg-6 order-lg-1">
            <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="d-block img-fluid mx-auto" alt="Image 1" style="width: 90%; height: 80%;">
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card" style="width: 100%; height: 80%; box-shadow: none;">  
                <div class="card-body">
                    <h5 class="card-title text-center">Card title</h5>
                    <p class="card-text">Rasanya sungguh melelahkan dan menjengkelkan. Segala sesuatu tampak sulit dan tidak sesuai harapan. Terkadang, hidup terasa seperti serangkaian tantangan yang tak kunjung usai. Kekecewaan dan rasa kesal seringkali menghampiri, membuat hati terasa berat. Namun, dalam kesulitan ini, kita perlu mencari kekuatan dan tekad untuk terus maju. Setiap kesalahan dan kegagalan adalah bagian dari perjalanan. Meski kata-kata kesal terus menggelayuti, mari hadapi dengan kepala dingin dan harapan bahwa setiap pagi membawa peluang baru untuk meraih kebahagiaan.</p>                
                </div>
            </div>
        </div>      
    </div>

    <div class="row justify-content-center">
        <div class="col-12 col-md-6 col-lg-6">
            <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="d-block img-fluid mx-auto" alt="Image 1" style="width: 90%; height: 80%;">
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <div class="card" style="width: 100%; height: 80%; box-shadow: none;"> 
                <div class="card-body">
                    <h5 class="card-title text-center">Card title</h5>
                    <p class="card-text">Rasanya sungguh melelahkan dan menjengkelkan. Segala sesuatu tampak sulit dan tidak sesuai harapan. Terkadang, hidup terasa seperti serangkaian tantangan yang tak kunjung usai. Kekecewaan dan rasa kesal seringkali menghampiri, membuat hati terasa berat. Namun, dalam kesulitan ini, kita perlu mencari kekuatan dan tekad untuk terus maju. Setiap kesalahan dan kegagalan adalah bagian dari perjalanan. Meski kata-kata kesal terus menggelayuti, mari hadapi dengan kepala dingin dan harapan bahwa setiap pagi membawa peluang baru untuk meraih kebahagiaan.</p>                
                </div>
            </div>
        </div>        
    </div>

    <!-- Card 5x1 Tarif Pengujian -->
    <div class="row justify-content-center px-0 px-md-5 px-lg-5">
        <div class="col-6 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">
                <div class="card-body text-center text-white py-3 px-0" style="background: #1C400B">
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">
                <div class="card-body text-center text-white py-3 px-0" style="background: #1C400B">
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">
                <div class="card-body text-center text-white py-3 px-0" style="background: #1C400B">
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">
                <div class="card-body text-center text-white py-3 px-0" style="background: #1C400B">
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">
                <div class="card-body text-center text-white py-3 px-0" style="background: #1C400B">
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                    <p class="card-text p-0 m-0">Tarif pengujian</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Card Standar Mutu ISO -->
    <div class="row justify-content-center px-0 px-md-5 px-lg-5 my-0">
        <div class="col-12">
        <h1 class="text-center fw-bolder position-relative d-flex justify-content-center" style="font-size=30px; font">STANDAR MUTU - ISO 
        <span class="position-absolute w-25" style=" height:.4rem; bottom:-1rem; background: #1C400B;"></span></h1>
        </div>
        <div class="col-12 py-5">
            <div class="row">
            <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" alt="...">              
            </div>
        </div>
        
            </div>
        </div>
    </div>
    
    <!-- Card Berita Terkini -->
    <div class="row justify-content-center px-0 px-md-5 px-lg-5 my-0">
        <div class="col-12">
            <h1 class="text-center fw-bolder position-relative d-flex justify-content-center" style="font-size=30px; font">BERITA TERKINI 
            <span class="position-absolute w-25" style=" height:.4rem; bottom:-1rem; background: #1C400B;"></span></h1>
        </div>
        <div class="col-12 py-5">
            <div class="row">
                <div class="col-6 col-md col-lg">
                <div class="card">
                        <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" style="height:200px" class="card-img-top" alt="...">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-white p-0 d-flex m-0" style="background-color: #1C400B; width: 100%;">
                                <div class="text-center px-2 py-1" style="width: 50%;">
                                    <small style="">dd/mm/yy</small>
                                </div>
                                <div class="px-1 py-1 text-center" style="background-color: #ffffff; width: 100%;">
                                    <small class="text-black p-0 m-0"><i class="far fa-user-circle p-0"></i>posted by</small>
                                </div>
                            </li>                             
                        </ul>
                        <div class="card-body text-black p-2" style="background: #DDEDEA">
                            <p class="card-text p-0 m-0">Tarif pengujian adalah tarif DDEDEA tarif DDEDEA tarif DDEDEA f DDEDEA tarif DDEDEA tarif DDEDEA</p>
                            <button class="mt-3 px-2 py-1 text-white" style="background-color: #1C400B;"><small>Baca lebih lanjut</small></button>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md col-lg">
                    <div class="card">
                        <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" style="height:200px" class="card-img-top" alt="...">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-white p-0 d-flex m-0" style="background-color: #1C400B; width: 100%;">
                                <div class="text-center px-2 py-1" style="width: 50%;">
                                    <small style="">dd/mm/yy</small>
                                </div>
                                <div class="px-1 py-1 text-center" style="background-color: #ffffff; width: 100%;">
                                    <small class="text-black p-0 m-0"><i class="far fa-user-circle p-0"></i>posted by</small>
                                </div>
                            </li>                             
                        </ul>
                        <div class="card-body text-black p-2" style="background: #DDEDEA">
                            <p class="card-text p-0 m-0">Tarif pengujian adalah tarif DDEDEA tarif DDEDEA tarif DDEDEA f DDEDEA tarif DDEDEA tarif DDEDEA</p>
                            <button class="mt-3 px-2 py-1 text-white" style="background-color: #1C400B;"><small>Baca lebih lanjut</small></button>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md col-lg">
                    <div class="card">
                        <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" style="height:200px" class="card-img-top" alt="...">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-white p-0 d-flex m-0" style="background-color: #1C400B; width: 100%;">
                                <div class="text-center px-2 py-1" style="width: 50%;">
                                    <small style="">dd/mm/yy</small>
                                </div>
                                <div class="px-1 py-1 text-center" style="background-color: #ffffff; width: 100%;">
                                    <small class="text-black p-0 m-0"><i class="far fa-user-circle p-0"></i>posted by</small>
                                </div>
                            </li>                             
                        </ul>
                        <div class="card-body text-black p-2" style="background: #DDEDEA">
                            <p class="card-text p-0 m-0">Tarif pengujian adalah tarif DDEDEA tarif DDEDEA tarif DDEDEA f DDEDEA tarif DDEDEA tarif DDEDEA</p>
                            <button class="mt-3 px-2 py-1 text-white" style="background-color: #1C400B;"><small>Baca lebih lanjut</small></button>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md col-lg">
                    <div class="card">
                        <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" style="height:200px" class="card-img-top" alt="...">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-white p-0 d-flex m-0" style="background-color: #1C400B; width: 100%;">
                                <div class="text-center px-2 py-1" style="width: 50%;">
                                    <small style="">dd/mm/yy</small>
                                </div>
                                <div class="px-1 py-1 text-center" style="background-color: #ffffff; width: 100%;">
                                    <small class="text-black p-0 m-0"><i class="far fa-user-circle p-0"></i>posted by</small>
                                </div>
                            </li>                             
                        </ul>
                        <div class="card-body text-black p-2" style="background: #DDEDEA">
                            <p class="card-text p-0 m-0">Tarif pengujian adalah tarif DDEDEA tarif DDEDEA tarif DDEDEA f DDEDEA tarif DDEDEA tarif DDEDEA</p>
                            <button class="mt-3 px-2 py-1 text-white" style="background-color: #1C400B;"><small>Baca lebih lanjut</small></button>
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>

    <!-- Card 3 video/image -->
    <div class="row justify-content-center px-0 px-md-5 px-lg-5">                     
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" style="height:250px" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" style="height:250px" alt="...">              
            </div>
        </div>
        <div class="col-4 col-md col-lg">
            <div class="card">
                <img src="https://likmi.ac.id/wp-content/uploads/2022/07/kesehatan.jpg" class="card-img-top" style="height:250px" alt="...">              
            </div>
        </div>        
    </div>
</div>

<?= $this->endSection() ?>